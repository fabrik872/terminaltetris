from clases import config
import sqlite3
from sqlite3 import Error


class dtb:
    conn = ''
    config = []

    def __init__(self):
        self.config = config.config.inst()
        try:
            self.conn = sqlite3.connect(self.config.get("sqlite")["file"])

        except Error as e:
            exit(e)

    def exec(self, sql, parameters={}):
        try:
            c = self.conn.cursor()
            c.execute(sql, parameters)
            self.conn.commit()
            return True
        except Error as e:
            print(e)
        return False

    def fetchAll(self, sql, parameters={}):
        try:
            c = self.conn.cursor()
            return c.execute(sql, parameters).fetchall()
        except Error as e:
            print(e)
        return False

    def fetchOne(self, sql, parameters={}):
        try:
            c = self.conn.cursor()
            return c.execute(sql, parameters).fetchone()
        except Error as e:
            print(e)
        return False

    def fetchMany(self, sql, size, parameters={}):
        try:
            c = self.conn.cursor()
            return c.execute(sql, parameters).fetchmany(size)
        except Error as e:
            print(e)
        return False

    def leaderBoard(self, page=0):

        offset = self.config.get("render")["playersOnPage"] * page

        return {
            "totalPages": self.fetchOne(self.config.get("sql")['countPlayers']),
            "page": self.fetchAll(self.config.get("sql")['selectPlayers'], {"offset": offset, "limit": self.config.get("render")["playersOnPage"]})
        }
