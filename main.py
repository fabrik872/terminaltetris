import re
import sys

from clases import frame
from clases import config
import time
import keyboard
import os
import pickle
from random import randint
from clases import dtb


class main:
    positions = []
    gameOver = False
    score = 0
    dtb = []
    render = []
    pageNum = 0
    fruitPosition = []
    direction = []
    x = 0
    y = 0
    cannotContinue = False
    config = []

    def __init__(self):
        self.config = config.config.inst()
        self.dtb = dtb.dtb()
        if not self.dtb.exec(self.config.get("sql")["createTables"]):
            exit('cannot create tables')

        self.render = frame.frame()
        self.x = self.config.get("render")["width"] // 2
        self.y = self.config.get("render")["height"] // 2
        self.direction = "right"
        self.fruitPosition = self.randomPosition()

    def main(self):

        fps = 0
        while True:
            timeToExecute = time.time()
            self.render.clear()
            os.system("cls||clear")
            self.game()
            print("FPS: " + str(fps))
            time.sleep(max(0, 1 / self.config.get("render")["difficulty"] - (time.time() - timeToExecute + 0.000675)))
            fps = round(1 / (time.time() - timeToExecute), 1)

    def game(self):

        if keyboard.is_pressed("right") and self.direction != "left":
            self.direction = 'right'
        if keyboard.is_pressed("left") and self.direction != "right":
            self.direction = 'left'
        if keyboard.is_pressed("up") and self.direction != "down":
            self.direction = 'up'
        if keyboard.is_pressed("down") and self.direction != "up":
            self.direction = 'down'

        self.x += self.config.get("render")["direction"][self.direction][0] * self.config.get("render")['speedX']
        self.y += self.config.get("render")["direction"][self.direction][1] * self.config.get("render")['speedY']

        if self.x >= self.config.get("render")["width"] - 1:
            self.x = 1
        if self.x <= 0:
            self.x = self.config.get("render")["width"] - 2
        if self.y >= self.config.get("render")["height"] - 1:
            self.y = 1
        if self.y <= 0:
            self.y = self.config.get("render")["height"] - 2

        self.positions.insert(0, [self.x, self.y, self.fruitPosition, self.score])

        if self.x == self.fruitPosition[0] and self.y == self.fruitPosition[1]:
            self.fruitPosition = self.randomPosition()
            self.score += 1

        self.render.setObject(self.config.get("render")["fruitModel"], self.fruitPosition[0], self.fruitPosition[1])

        for iteration, position in enumerate(self.positions[
                                             :self.config.get("render")["defaultTail"] + self.score *
                                              self.config.get("render")[
                                                  "tailIncrement"]]):
            self.render.setObject(self.config.get("render")["snakeTailModel"], position[0], position[1])
            if position[0] == self.x and position[1] == self.y and iteration > 1:
                self.render.setBigObject(["GAME OVER"], self.config.get("render")["width"] // 2 - 4,
                                         self.config.get("render")["height"] // 2)
                self.gameOver = True

        self.render.setObject(self.config.get("render")["snakeHeadModel"], self.x, self.y)

        self.render.setScore(self.score)
        self.render.execute()
        if keyboard.is_pressed('q') or self.gameOver:
            self.menu()

    def randomPosition(self):
        return [randint(2, self.config.get("render")["width"] - 2), randint(2, self.config.get("render")["height"] - 2)]

    def menu(self):
        if self.gameOver and not self.cannotContinue:
            sys.stdout.flush()
            name = input("Enter your name: ")
            if (self.dtb.exec(self.config.get("sql")["insertPlayer"],
                              {'name': name, 'score': self.score, "tracker": pickle.dumps(self.positions)})):
                print('Game saved')
                self.cannotContinue = True

            else:
                if input("Problem while saving game try again? [y/n]: ") == "y":
                    self.menu()
                else:
                    exit("bye")

        os.system("cls||clear")
        self.render.execute()
        print("MENU\n")
        print("1. Continue")
        print("2. Show Leader board")
        print("3. Show Game history")
        print("4. Save and exit Game")
        print("5. Exit Game")
        option = input("\n Select option: ")

        if option == "4":
            if self.cannotContinue:
                exit("Game already saved bye")
            self.gameOver = True


        elif option == "5":
            exit("bye")

        elif option == "1":
            if self.gameOver:
                print("Game Over cannot continue")
                self.menu()
            self.main()

        elif option == "3":
            self.showGameHistory()

        elif option == "2":
            board = ["Leader Board", "", "#    name             score"]

            for itt, page in enumerate(self.dtb.leaderBoard(self.pageNum)["page"]):
                orderId = itt + self.config.get("render")["playersOnPage"] * self.pageNum

                row = str(orderId)
                row += str(self.spaces(row, 4)) + page[0]
                row += str(self.spaces(row, 24)) + str(page[1])
                board.append(row)

            self.render.clear()
            self.render.setBigObject(board, 2, 2)
        else:
            print("Unknown command")
        self.menu()

    @staticmethod
    def spaces(data, margin):
        spaces = margin - len(data)
        spaceStr = " "

        if spaces < 0:
            return spaceStr
        else:
            for space in range(spaces):
                spaceStr += " "
        return spaceStr

    def showGameHistory(self):
        board = ["Pick game", "", "id   name             score"]

        for itt, page in enumerate(self.dtb.leaderBoard(self.pageNum)["page"]):
            row = str(page[3])
            row += str(self.spaces(row, 4)) + page[0]
            row += str(self.spaces(row, 24)) + str(page[1])
            board.append(row)

        self.render.clear()
        self.render.setBigObject(board, 2, 2)
        os.system("cls||clear")
        self.render.execute()

        playerId = input("Player ID: ")
        playerData = self.dtb.fetchOne(self.config.get("sql")["selectOnePlayerById"], {"id": int(playerId)})
        trackerData = pickle.loads(playerData[3])
        snakePositions = []

        for position in trackerData[::-1]:
            snakePositions.insert(0, position)
            snakePositions = snakePositions[
                             :self.config.get("render")["defaultTail"] + position[3] * self.config.get("render")[
                                 "tailIncrement"]]

            self.render.clear()
            self.render.setObject(self.config.get("render")["fruitModel"], position[2][0], position[2][1])
            for snakePosition in snakePositions:
                self.render.setObject(self.config.get("render")["snakeTailModel"], snakePosition[0], snakePosition[1])

            self.render.setObject(self.config.get("render")["snakeHeadModel"], position[0], position[1])

            os.system("cls||clear")
            self.render.execute()
            print("Press Q for exit")
            if keyboard.is_pressed('q'):
                self.menu()


if __name__ == "__main__":
    main().main()
