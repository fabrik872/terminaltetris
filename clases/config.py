import json


class config:
    jsonData = []
    instance = None

    @staticmethod
    def inst():
        if config.instance is None:
            object = config()
            config.instance = object

        return config.instance

    def __init__(self):
        self.jsonData = json.loads(open('config.json', 'r+').read())
        if not config.instance == None:
            exit('Only one instance of TestSingleton is allowed!')

    def get(self, name):
        return self.jsonData[name]
