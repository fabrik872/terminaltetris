from clases import config


class frame:
    data = [[]]
    score = 0
    config = []

    def __init__(self):
        self.config = config.config.inst()
        self.clear()

    def clear(self):
        frameData = []
        # Top line
        for row in range(0, self.config.get('render')['height']):
            frameData.append([])
            for i in range(0, self.config.get('render')['width']):
                if row == 0:
                    frameData[row].append(self.config.get('render')['topLine'])
                elif row >= self.config.get('render')['height'] - 1:
                    frameData[row].append(self.config.get('render')['bottomLine'])
                else:
                    if i == 0:
                        frameData[row].append(self.config.get('render')['leftLine'])
                    elif i == self.config.get('render')['width'] - 1:
                        frameData[row].append(self.config.get('render')['rightLine'])
                    else:
                        frameData[row].append(' ')
        self.data = frameData

    def setScore(self, score):
        self.score = score

    def setObject(self, model, x, y):
        self.data[y][x] = model

    def setBigObject(self, model, x, y):

        for rowKey, row in enumerate(model):
            rowKey = rowKey + y

            for columnKey, column in enumerate(row):
                columnKey = columnKey + x
                self.data[rowKey][columnKey] = column

    def execute(self):
        toPrint = ''
        for row in self.data:
            for i in row:
                toPrint += i
            toPrint += '\n'
        toPrint += "Score: " + str(self.score)
        print(toPrint)
        print("'up', 'down', 'left', 'right' -> movement; 'q' -> pause/exit game")
        # pprint(self.config.get('render')['width'])
